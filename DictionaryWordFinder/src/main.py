#!/usr/bin/env python
# -*- coding: latin-1 -*-

__author__ = "Cristian Tejedor Garc�a"
__credits__ = ["Cristian Tejedor Garc�a"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Cristian Tejedor Garc�a"
__email__ = "cristian [at] infor [dot] uva [dot] es"
__status__ = "Production"

'''
Created on 25/03/2017

It shows the maximum, minimum and specific line length of an input file.

@author: Cristian TG @weaver7x
@since: 2017/03/25
'''

# File path of the input dictionary by default
FILE_INPUT = "spanish.txt"
# File path of teh result file by default
FILE_OUTPUT = "result.txt"
# Size of the lines looking for by default
SPEC_SIZE = 20
# CUSTOM SEPARATOR OF EACH ELEMENT OF THE LISTS
SEPARATOR = ' '


def maxWord(fileName=FILE_INPUT):
    '''
    @param fileName: File path of the input dictionary.
    @return A dictionary with one entry: size and words with that size.
    Gets a list the size and a list of words without repeating with as many characters as possible.
    '''
    size = 0
    words = set()
    with open(fileName, "r") as f:
        while True:
            line = f.readline().strip()
            if not line: break
            if size == len(line):
                words.add(line)
            elif size < len(line):
                size = len(line)
                words.clear()
                words.add(line)
    return [size, sorted(words)]


def minWord(fileName=FILE_INPUT, spec_size=1):
    '''
    @param fileName: File path of the input dictionary.
    @param spec_size: Limit number of characters to search.
    @return A dictionary with one entry: size and words with that size.
    Gets a list of the size and a list of words without repeating with as few characters as possible.
    '''
    size = 99
    words = set([])
    with open("spanish.txt", "r") as f:
        while True:
            line = f.readline().strip()
            if not line: break
            if size == len(line):
                words.add(line)
            elif len(line) < size and len(line) >= spec_size:
                size = len(line)
                words.clear()
                words.add(line)
    return [size, sorted(words)]


def main():
    '''
    It displays all results on screen.
    It saves the results to a file.
    '''

    mLists = {"Max: ":maxWord(), '\nMin: ':minWord(), '\nSpecific: ':minWord(spec_size=SPEC_SIZE)}
    result = ""
    for key, value in mLists.iteritems():
        result += key 
        result += str(value[0]) + ':'
        for i in value[1]:
            result += SEPARATOR + i
    
    # 1. Console output 
    print result
    # 2. File output
    with open(FILE_OUTPUT, "w") as f:
        f.write(result)
        

    return None



if __name__ == '__main__':
    main()