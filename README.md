# DictionaryWordFinder #

Simple python project for looking for some specific words in a dictionary file.



# RUNNING #

Simply run main.py



# PROJECT FORMAT #

It is compatible with a pydev Eclipse project.



# ENCODING #

All .py files and the dictionary file: LATIN-1 (ISO8859-1).

Please feel free to change it depending on your language text file.



### Who do I talk to? ###

* Cristian TG @weaver7x cristian [at] infor [dot] uva [dot] es